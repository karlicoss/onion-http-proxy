This project is my toy implementation of the [onion routing](https://en.wikipedia.org/wiki/Onion_routing) protocol and a simple HTTP proxy as an example.

**Disclaimer**: this code is quite outdated and certainly not pretty. I wish I could find time to rewrite it :)

# Onion rounting in a nutshell
Onion routing basically uses a chain of relay servers as a channel for secure communication. In this chain, every
router 'peels' an encryption layer from the packet to figure out where to pass it, which explains 'onion' in the
protocol name.

Suppose node A tries to establish secure connection with node B. In order to do that, A selects a chain of routers
(e.g. randomly), which will relay its packets to B. A sets up an initial connection with each of the nodes in the
chain, and establishes a symmetric key with each of them using the Diffie-Hellman key exchange protocol.

Some important facts:

1. Intermediate routers/nodes don't know each others' symmetic keys.
2. What is more, the only node aware of connection setup is the one which is being connected. All the previous
nodes in chain are just relaying some encrypted data.
3. After establishing initial connection, all the data from node A will be encrypted with its symmetric key, that
 is, intermediate routers will not be able to decrypt or even extract any meta from the packets.

These properties not only make decrypting data complex, but even make really hard tracking who sent the data and
who is the receiver, which is why compromising few onion routers is not enough to compromise the whole network.

# Building
You don't need anything except Java 8.

Build everything with:

    ./gradlew relayJar directoryJar proxyJar

# Running
To run all components at once on the local machine:

1. Check if you are happy with the default ports in `run.sh`.
2. Run (in separate terminals)
    1. `./run.sh directory` (runs the directory which helps relay nodes to discover each other)
    2. `./run.sh proxy` (runs the HTTP proxy)
    3. `./run.sh relay 1111`, `./run.sh relay 1112`, ... (as many as you wish, just use
    different ports)
3. Now, try proxying something, e.g. `curl -x localhost:10000 google.com`. You should receive
a response and see some stuff happening in logs.

# Features and implementation details

## HTTP proxy
In theory, such a channel could be used to communicate any data. As an example, I implemented a simple HTTP Proxy,
 with GET requests support. Proxy is being run on a local machine and establishes the connection with a remote
 router via a chain of intermediate nodes. After that, every request to the proxy is being sent via the secure
 channel.

The proxy keeps multiple relay chains and drops ones which were used a lot for more security.
The implementation is quite flexible and lets establishing multiple connections at once,
 using a random relay chain,timeouts, etc.

You can find the source code for the proxy in class `proxy.HttpProxyServer` and the `onion` package.

## Asynchronicity
The routers process the requests asynchronously:

1. IO operations are being processed on separate thread pool, thanks to [Netty](http://netty.io/) framework.
2. Costy computation (e.g. encryption and decryption) run on separate threads and implemented
 via Java 8 `CompletableFuture`s.

Because of that, routers potentially able to handle multiple connections and make progress
 in each of these connections.


## Encryption
* Asymmetric encryption which is used for establishing initial connection is 4096 bits RSA
* Symmetric encryption is the `AES/ECB/PKCS5Padding` algorithm with the 128 bits key