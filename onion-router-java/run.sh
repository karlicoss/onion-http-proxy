#!/bin/bash
set -e
set -x

JAVA8="/usr/lib/jvm/java-8-oracle/bin/java"

# relay nodes directory, potentially could be on another machine
DIRECTORY_HOSTNAME="localhost"
DIRECTORY_PORT=5000 

# number of intermediate relay nodes
CHAIN_LENGTH=3

# local port for HTTP proxy
PROXY_PORT=10000


what="$1" # what should we run?

case $what in
"directory")
    $JAVA8 -jar build/libs/directory.jar $DIRECTORY_PORT
    ;;
"proxy")
    $JAVA8 -jar build/libs/proxy.jar $PROXY_PORT $CHAIN_LENGTH $DIRECTORY_HOSTNAME $DIRECTORY_PORT
    ;;
"relay")
    relay_port="$2"
    $JAVA8 -jar build/libs/relay.jar $relay_port $DIRECTORY_HOSTNAME $DIRECTORY_PORT
    ;;
*)
    echo "Unknown command!"
    exit 1
    ;;
esac


# $JAVA8 -cp onion-router.jar proxy.HttpProxyServer $PROXY_PORT $CHAIN_LENGTH $DIRECTORY_HOSTNAME $DIRECTORY_PORT