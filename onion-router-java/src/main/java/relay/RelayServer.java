package relay;

import crypto.Constants;
import crypto.DiffieHellman;
import directory.DirectoryServer;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import onion.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import static crypto.DiffieHellman.secretToBytes;
import static tools.Tools.errprintln;


/**
 * The actual relay server
 */
public class RelayServer extends OnionServer {
    public final String hostname; // TODO not very cool...
    {
        String tmp;
        try {
            tmp = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            tmp = null;
        }
        hostname = tmp;
    }
    public final int port;

    private final static int DIRECTORY_NOTIFY_INTERVAL_SECS = 30;
    private final InetSocketAddress directoryAddress;

    private final HashMap<Token, byte[]> keys = new HashMap<>();

    private static final Cipher rsaCipher;
    static {
        Cipher tmp = null;
        try {
            tmp = Cipher.getInstance("RSA");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException r) {
            // should not happen!
            r.printStackTrace();
        }
        rsaCipher = tmp;
    }


    private final PrivateKey privateKey;
    private final PublicKey publicKey;
    private final byte[] bpublicKey;

    public RelayServer(int port, InetSocketAddress directoryAddress) {
        super(5, 5);
        this.port = port;
        this.directoryAddress = directoryAddress;

        debug("Generating RSA key pair...");
        // key generation
        Security.addProvider(new BouncyCastleProvider());// TOOD where it should be?
        KeyPairGenerator g;
        try {
            g = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e1) {
            // this totally should not happed
            e1.printStackTrace();
            g = null;
        }
        g.initialize(Constants.RSA_KEY_STRENGTH_BITS);
        KeyPair keyPair = g.generateKeyPair();
        this.privateKey = keyPair.getPrivate();
        this.publicKey = keyPair.getPublic();
        this.bpublicKey = this.publicKey.getEncoded();
        debug(String.format("Public RSA key: %s ...", Arrays.toString(Arrays.copyOf(this.bpublicKey, 10))));
    }

    private void addKey(Token incoming, byte[] key) {
        keys.put(incoming, key);
    }

    private byte[] getKey(Token incoming) {
        return keys.get(incoming);
    }

    public InetSocketAddress getAddress() {
        //            InetAddress localhost = InetAddress.getLocalHost();
//            return new InetSocketAddress(localhost, port);
        return InetSocketAddress.createUnresolved("localhost", port);
    }

    private byte[] rsaDecrypt(byte[] edata) {
        try {
            rsaCipher.init(Cipher.DECRYPT_MODE, privateKey);
            return rsaCipher.doFinal(edata);
        } catch (BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
            e.printStackTrace();
            throw new RuntimeException("THIS SHOULD NOT HAVE HAPPENED");
        }
    }

    /**
     * ecdata contains RSA-encrypted Diffie-Hellman hanshake part
     */
    private CompletableFuture<BackwardOuterPacket> createConnection(Token incoming, byte[] ecdata) {
        byte[] botherPublicSecret = rsaDecrypt(ecdata);
        BigInteger otherPublicSecret = new BigInteger(botherPublicSecret);
        debug(String.format("Connection request, A = %s", otherPublicSecret));

        return CompletableFuture.supplyAsync(() -> {
            addRoute(incoming, null);
            BigInteger privateSecret = DiffieHellman.generatePrivateSecret();
            BigInteger publicSecret = DiffieHellman.publicSecret(privateSecret);
            byte[] dh2 = publicSecret.toByteArray();
            BigInteger key = DiffieHellman.getKey(privateSecret, otherPublicSecret);
            debug("Acquired symmetric key: " + key);
            byte[] bkey = secretToBytes(key);
//            byte[] bytes = new BackwardInnerPacket(getKey()).getBytes();

            addKey(incoming, bkey);
            return new BackwardOuterPacket(BackwardResponseType.CREATED, Token.fakeToken(), dh2);
        }, compExecutor);
    }

    public void runDirectoryNotifier() {
        debug("RUNNING DIRECTORY NOTIFIER");
        while (true) {
            try {
                Socket socket = new Socket();
                socket.connect(directoryAddress);

                DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

                dos.writeInt(DirectoryServer.QueryType.REGISTER.ordinal());

                byte[] bhostname = hostname.getBytes(Charset.forName("UTF-8"));
                dos.writeInt(bhostname.length);
                dos.write(bhostname);

                dos.writeInt(port);

                dos.writeInt(bpublicKey.length);
                dos.write(bpublicKey);

                socket.close();
                debug("Notified the directory");
                Thread.sleep(DIRECTORY_NOTIFY_INTERVAL_SECS * 1000);
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void run() {
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup serverGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, serverGroup).
                    channel(NioServerSocketChannel.class).
                    childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            socketChannel.pipeline().addLast(new OuterPacketEncoder());
                            socketChannel.pipeline().addLast(new OuterPacketDecoder(PacketType.FORWARD));
                            socketChannel.pipeline().addLast(new ServerHandler(compExecutor));
                        }
                    }).option(ChannelOption.SO_BACKLOG, 128).
                    childOption(ChannelOption.SO_KEEPALIVE, true);
            ChannelFuture f = b.bind(this.port).sync();
            debug(String.format("RUNNING RELAY SERVER ON PORT %d, DIRECTORY %s", this.port, this.directoryAddress));
            new Thread(this::runDirectoryNotifier).start();
            f.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            serverGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    @Override
    protected void debug(String message) {
        errprintln(String.format("[Relay server %d] %s", port, message));
    }

    private class ServerHandler extends SimpleChannelInboundHandler<OuterPacket> {
        private final Executor compExecutor;

        public ServerHandler(Executor compExecutor) {
            this.compExecutor = compExecutor;
        }

        private void debug(String message) {
            System.err.println(String.format("[Server %s] %s", "TODO IDENTIFIER", message));
        }

        /**
         * Decides if we need to relay or create the connection and processes the packet correspondingly
         * @param packet
         * @return Promise of the response
         */
        private CompletableFuture<BackwardOuterPacket> process(ForwardOuterPacket packet) {
            if (packet.getTp() == ForwardCommandType.CREATE)
                return createConnection(packet.getToken(), packet.getData());
            else { // RELAY
                return relay(packet.getToken(), packet.getData(), getKey(packet.getToken()));
            }
        }

        @Override
        protected void messageReceived(ChannelHandlerContext ctx, OuterPacket outerPacket) throws Exception {
//            debug("Local: " + ctx.channel().localAddress().toString());
//            debug("Remote: " + ctx.channel().remoteAddress().toString());
            ForwardOuterPacket packet = (ForwardOuterPacket) outerPacket;
//            debug("Received " + packet);
            CompletableFuture<BackwardOuterPacket> f = process(packet);
            f.thenAcceptAsync((BackwardOuterPacket response) -> {
//                debug("Promise executed");
//                debug("Sending back " + response);
                ctx.writeAndFlush(response);
            }, compExecutor);
        }
    }

    public static void main(String[] args) {
        if (args.length != 3) {
            throw new IllegalArgumentException("Usage: java RelayServer serverPort directoryHostname directoryPort");
        }
        int port = Integer.parseInt(args[0]);
        String directoryHostname = args[1];
        int directoryPort = Integer.parseInt(args[2]);
        InetSocketAddress directoryAddress = new InetSocketAddress(directoryHostname, directoryPort);
        RelayServer server = new RelayServer(port, directoryAddress);
        server.run();
    }
}
