package onion;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class OuterPacket {
    private final int tp;
    private final Token token;
    private final byte[] data;
    public OuterPacket(int tp, Token token, byte[] data) {
        this.tp = tp;
        this.token = token;
        this.data = data;
    }

    public byte[] getBytes() {
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        DataOutputStream ds = new DataOutputStream(bs);
        try {
            ds.writeInt(tp);
            ds.write(token.getBytes());
//            System.out.println(Arrays.toString(token.getBytes()));
            ds.write(data);
            ds.close();
            return bs.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("THIS SHOULD NOT HAVE HAPPENED");
        }
    }

    public static OuterPacket fromBytes(byte[] bytes, PacketType ptype) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        int tp = bb.getInt();
        byte[] btoken = new byte[Token.bytesSize()];
        bb.get(btoken);
        Token token = Token.fromBytes(btoken);
        byte[] data = new byte[bb.remaining()];
        bb.get(data);
        if (ptype == PacketType.FORWARD) {
            return new ForwardOuterPacket(ForwardCommandType.values()[tp], token, data);
        } else { // BACKWARD
            return new BackwardOuterPacket(BackwardResponseType.values()[tp], token, data);
        }

    }
}