package onion;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Token {
    private final int token;

    public Token(int token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "Token{" +
                "token=" + token +
                '}';
    }

    public byte[] getBytes() {
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        DataOutputStream ds = new DataOutputStream(bs);
        try {
            ds.writeInt(token);
            return bs.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("THIS SHOULD NOT HAVE HAPPENED");
        }
    }

    public static Token fakeToken() {
        return new Token(-1);
    }

    public static Token fromBytes(byte[] bytes) {
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        int token = bb.getInt();
        return new Token(token);
    }

    public static int bytesSize() {
        return 4;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Token token1 = (Token) o;

        if (token != token1.token) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return token;
    }
}
