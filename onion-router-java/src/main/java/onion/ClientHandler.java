package onion;

import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.ChannelHandlerContext;

import java.util.concurrent.CompletableFuture;

import static tools.Tools.errprintln;

/**
 * Sends an outer packet (either Forward or Backward) and stores the response into a future
 */
class ClientHandler extends SimpleChannelInboundHandler<OuterPacket> {
    private final OuterPacket opacket;
    public final CompletableFuture<BackwardOuterPacket> rpacket;

    public ClientHandler(OuterPacket opacket) {
        this.opacket = opacket;
        this.rpacket = new CompletableFuture<>();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
//        debug("Sending packet " + this.opacket);
        ctx.writeAndFlush(this.opacket);
    }

    private void debug(String message) {
        errprintln("[Client TODO ID] " + message);
//        errprintln(String.format("[Server %d] %s", this.token, message));
    }

    @Override
    protected void messageReceived(ChannelHandlerContext ctx, OuterPacket outerPacket) throws Exception {
        BackwardOuterPacket packet = (BackwardOuterPacket)outerPacket;
        rpacket.complete(packet);

//        debug("Received a response " + packet);
//        debug("Closing channel...");
        ctx.close();
    }
}
