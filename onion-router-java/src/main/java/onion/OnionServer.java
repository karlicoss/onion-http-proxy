package onion;

import crypto.Tools;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.apache.commons.lang3.tuple.Pair;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.SocketAddress;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.util.*;
import java.util.concurrent.*;

/**
 * Base class for the relay server.
 * Onion proxy extends it too for the sake of reusing routes, asynchronicity and all that stuff.
 * token is server ID
 * token is curcuit ID
 */
public abstract class OnionServer {
    /**
     * Executor for asynchronous non-IO computations
     */
    protected final Executor compExecutor;
    /**
     * Kind of executor for IO
     */
    private final NioEventLoopGroup clientGroup;
    /**
     * Set of tokens we've used
     */
    private Set<Token> used = new HashSet<>();
    /**
     * Mapping token -> (nextAddress, nextToken)
     * If the value of the mapping is null, that means that current server is the endpoint and will serve as a proxy for DATA packets.
     * Otherwise, DATA packets get relayed to the nextAddress server through the token nextToken
     */
    protected final HashMap<Token, Pair<InetSocketAddress, Token>> routes = new HashMap<>();

    protected OnionServer(int cThreads, int ioThreads) {
        this.compExecutor = Executors.newFixedThreadPool(cThreads);
        this.clientGroup = new NioEventLoopGroup(ioThreads);
    }

    protected abstract void debug(String message);

    protected void addRoute(Token incoming, Pair<InetSocketAddress, Token> route) {
        used.add(incoming); // TODO quick fix fox loops...
        routes.put(incoming, route);
    }

    protected Pair<InetSocketAddress, Token> getRoute(Token incoming) {
        if (!routes.containsKey(incoming)) {
            System.err.println(routes);
            throw new RuntimeException("No key " + incoming);
        } else {
            return routes.get(incoming);
        }
    }

    /**
     * Creates and runs the client which sends the packet to the given address
     * @param address
     * @param packet
     * @return Promise of response from the server we were connecting to.
     */
    protected CompletableFuture<BackwardOuterPacket> makeClient(SocketAddress address, OuterPacket packet) {
        debug("Running client!");
        Bootstrap b = new Bootstrap();
        b.group(this.clientGroup);
        b.channel(NioSocketChannel.class);
        b.option(ChannelOption.SO_KEEPALIVE, true);
        ClientHandler chandler = new ClientHandler(packet);
        b.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                socketChannel.pipeline().addLast(new OuterPacketEncoder());
                socketChannel.pipeline().addLast(new OuterPacketDecoder(PacketType.BACKWARD));
                socketChannel.pipeline().addLast(chandler);
            }
        });
        ChannelFuture f = b.connect(address);
        debug("Connected to " + address);
        return CompletableFuture.runAsync(() -> {
            try {
                f.channel().closeFuture().sync();
                debug("Completed!");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, compExecutor).
                thenApplyAsync((Void) -> chandler.rpacket.join(), compExecutor);
    }

    /**
     * At this point we know we don't have to extend the connection, therefore we either serve as HTTP proxy, or relay it further.
     * @param incoming incoming token through which the data was sent
     * @param data the data which was sent
     * @return Promise of response from the onion chain tail.
     */
    protected CompletableFuture<BackwardInnerPacket> passData(Token incoming, byte[] data) {
        Pair<InetSocketAddress, Token> route = getRoute(incoming);
        if (route == null) {
//            debug(String.format("Sending %s to the Internet", Arrays.toString(data)));
            try {
                Callable<byte[]> client = new HttpRequestHandler(data).makeRequest();
                return CompletableFuture.supplyAsync(() -> {
                    try {
                        byte[] response = client.call();
                        BackwardInnerPacket ripacket = new BackwardInnerPacket(response);
                        debug("Responding back...");
                        return ripacket;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }, compExecutor);
            } catch (MalformedURLException e) {
                return CompletableFuture.supplyAsync(() -> {
                    String rstring = "BAD URL: " + e.getMessage();
                    byte[] response = rstring.getBytes(Charset.forName("UTF-8"));
                    BackwardInnerPacket ripacket = new BackwardInnerPacket(response);
                    return ripacket;
                }, compExecutor);
            }
        } else {
            InetSocketAddress address = route.getLeft();
            Token outgoing = route.getRight();

            ForwardOuterPacket outPacket = new ForwardOuterPacket(ForwardCommandType.RELAY, outgoing, data);

            CompletableFuture<BackwardOuterPacket> cf = makeClient(address, outPacket);
            return cf.thenApplyAsync(
                    (BackwardOuterPacket packet) -> {
//                        debug("Responding with " + packet);
                        return new BackwardInnerPacket(packet.getData());
                    },
                    compExecutor);
        }
    }

    /**
     * // Decides if the received data was extend query or data packet and processes it appropriately.
     * @param incoming Incoming token
     * @param edata Incoming data (encrypted)
     * @param key
     * @return Promise of response from the onion chain tail.
     */
    protected CompletableFuture<BackwardOuterPacket> relay(Token incoming, byte[] edata, byte[] key) {
//        debug("Relaying " + Arrays.toString(edata) + " from " + incoming);
        byte[] data = decrypt(edata, key);
        ForwardInnerPacket ipacket = ForwardInnerPacket.Companion.fromBytes(data);
        CompletableFuture<BackwardInnerPacket> future;
        if (ipacket.getTp() == ForwardInnerPacketType.EXTEND) {
//            debug("Extending: " + ipacket);
            future = extendConnection(incoming, ipacket.getAddress(), ipacket.getData());
        } else { // DATA
//            debug("Passing data: " + ipacket);
            future = passData(incoming, ipacket.getData());
        }
        return future.thenApplyAsync((BackwardInnerPacket p) ->
                new BackwardOuterPacket(
                        BackwardResponseType.RELAY,
                        Token.fakeToken(), encrypt(p.getBytes(), key)),
                compExecutor);
    }


    private Token generateToken() {
        int cur = 0;
        while (true) {
            Token token = new Token(cur);
            if (used.contains(token)) {
                cur += 1;
            } else {
                used.add(token);
                return token;
            }
        }
    }

    /**
     * Extends the connection like: incomingToken -> (nextAddress, outgoingToken)
     * @param incoming Incoming token
     * @param nextAddress Address of the server we want to extend to. Unresolved.
     * @param data Necessary data for connection establishing.
     * @return Promise of response from the onion chain tail.
     */
    protected CompletableFuture<BackwardInnerPacket> extendConnection(Token incoming, InetSocketAddress nextAddress, byte[] data) {
        Token nextToken = generateToken();
        ForwardOuterPacket packet = new ForwardOuterPacket(ForwardCommandType.CREATE, nextToken, data);
        debug(String.format("Extending to the node %s:%d", nextAddress.getHostName(), nextAddress.getPort()));
        InetSocketAddress resolved = new InetSocketAddress(nextAddress.getHostName(), nextAddress.getPort()); // resolving
//        debug(String.format("Resolved: %s", resolved));
        CompletableFuture<BackwardOuterPacket> future = makeClient(resolved, packet);
        return future.thenApplyAsync((BackwardOuterPacket p) -> {
            debug("Extended successfullly, adding route");
            addRoute(incoming, Pair.of(resolved, nextToken));
            return new BackwardInnerPacket(p.getData()); // TODO
        }, compExecutor);
    }

    protected byte[] encrypt(byte[] data, byte[] key) {
        if (key == null)
            return data;

        try {
            Cipher cipher = Tools.getAesCipher();
            cipher.init(Cipher.ENCRYPT_MODE, Tools.getAesSecretKey(key));
            byte[] edata = cipher.doFinal(data);
            return edata;
        } catch (BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
            throw new RuntimeException("THIS SHOULD NOT HAVE HAPPENED", e);
        }

//        byte[] edata = new byte[2 + data.length];
//        edata[0] = key[0];
//        edata[1] = key[1];
//        for (int i = 0; i < data.length; i++) {
//            edata[2 + i] = data[i];
//        }
//        return edata;
    }

    protected byte[] decrypt(byte[] edata, byte[] key) {
        if (key == null) {
            return edata;
        }
        try {
            Cipher cipher = Tools.getAesCipher();
            cipher.init(Cipher.DECRYPT_MODE, Tools.getAesSecretKey(key));
            byte[] data = cipher.doFinal(edata);
            return data;
        } catch (BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
            throw new RuntimeException("THIS SHOULD NOT HAVE HAPPENED", e);
        }
//
//        if (!(edata[0] == key[0] && edata[1] == key[1])) {
//            throw new RuntimeException("Edata: " + Arrays.toString(edata) + ", key: " + Arrays.toString(key));
//        }
//        return Arrays.copyOfRange(edata, 2, edata.length);
    }

}