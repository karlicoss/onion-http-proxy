package onion;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.nio.charset.Charset;
import java.util.concurrent.Callable;

/**
 * Sets up a HTTP connection and executes the GET query
 */
public class HttpRequestHandler {
    private static final int BUFFER_SIZE = 32768; // TODO
    private URL url = null;

    public HttpRequestHandler(byte[] request) throws MalformedURLException {
        this.url = new URL(new String(request, Charset.forName("UTF-8")));
    }

    private void debug(String message) {
        System.err.println("[HTTP Request handler] " + message);
    }

    public Callable<byte[]> makeRequest() {
        return () -> {
            try {
                debug(String.format("Sending GET request %s to the internet", url.toString()));
                URLConnection conn = url.openConnection();
                conn.setDoInput(true);
                conn.setDoOutput(false); // disable POST
                HttpURLConnection huc = (HttpURLConnection)conn;
                try (InputStream in = huc.getInputStream()) {
                    byte data[] = new byte[BUFFER_SIZE];

                    ByteArrayOutputStream os = new ByteArrayOutputStream();

                    int cnt = in.read(data, 0, BUFFER_SIZE);
                    while (cnt != -1)
                    {
                        os.write(data, 0, cnt);
                        cnt = in.read(data, 0, BUFFER_SIZE);
                    }
                    os.flush(); // I guess I don't actually need to, but anyway
                    debug(String.format("Evaluated the GET request"));
                    return os.toByteArray();
                }
            } catch (IOException e) {
                byte[] response = "ERROR: Unknown host".getBytes(Charset.forName("UTF-8")); // TODO consider more cases
                debug(String.format("ERROR: Unknown host"));
                return response;
            }
        };
    }

}
