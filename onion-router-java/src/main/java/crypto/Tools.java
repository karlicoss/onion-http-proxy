package crypto;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;

public class Tools {
    public static KeyFactory getRsaKeyFactory() {
        KeyFactory tmp = null;
        try {
            tmp = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new RuntimeException("THIS SHOULD NOT HAVE HAPPENED");
        }
        return tmp;
    }

    public static SecretKeySpec getAesSecretKey(byte[] secret) {
        SecretKeySpec secretKey = new SecretKeySpec(secret, "AES");
        return secretKey;
    }

    public static Cipher getAesCipher() {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            return cipher;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            e.printStackTrace();
            throw new RuntimeException("THIS SHOULD NOT HAVE HAPPENED");
        }
    }
}
