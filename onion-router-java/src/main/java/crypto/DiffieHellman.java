package crypto;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Random;

/**
 * Two parties: Alice and Bob, they both know prime and generator
 * Both of them have a secret value sA and sB
 * Alice generates public secret A = g^sa mod p and sends A to Bob.
 * Bob generates public secret B = g^sb mod p and sends B to Alice
 * TODO tests
 */
public class DiffieHellman {
    // Prime and generator borrowed from from http://tools.ietf.org/html/rfc3526#section-3
    public static final BigInteger prime = new BigInteger("FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF", 16);
    public static final BigInteger generator = BigInteger.valueOf(2);


    public static BigInteger publicSecret(BigInteger privateSecret) {
        return generator.modPow(privateSecret, prime);
    }

    public static BigInteger getKey(BigInteger privateSecret, BigInteger otherPublicSecret) {
        return otherPublicSecret.modPow(privateSecret, prime);
    }

    /**
     * TODO not very good to use standard random generator
     * @return
     */
    public static BigInteger generatePrivateSecret() {
        return new BigInteger(Constants.AES_KEY_SIZE_BITS, new Random());
    }

    public static byte[] secretToBytes(BigInteger secret) {
        byte[] bsecret = secret.toByteArray();
        if (bsecret.length >= Constants.AES_KEY_SIZE_BITS / 8) {
            return Arrays.copyOfRange(bsecret, 0, Constants.AES_KEY_SIZE_BITS / 8);
        } else {
            byte[] res = new byte[Constants.AES_KEY_SIZE_BITS / 8];
            for (int i = 0; i < bsecret.length; i++) {
                res[i] = bsecret[i];
            }
            return res;
        }
    }
}