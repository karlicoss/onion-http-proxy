package crypto;


public class Constants {
    public static final int RSA_KEY_STRENGTH_BITS = 4096;
    public static final int AES_KEY_SIZE_BITS = 128;
}
