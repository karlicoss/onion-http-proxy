package directory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static tools.Tools.errprintln;

/**
 * Stores the index of known relay nodes
 */
public class DirectoryServer {
    public final int port;
    private final Executor executor;

    private static final long EXPIRED_TIME_SECONDS = 40;

    private final Map<ServerInfo, Long> directory;

    public DirectoryServer(int port, int nThreads) {
        this.port = port;
        this.executor = Executors.newFixedThreadPool(nThreads);
        this.directory = new ConcurrentSkipListMap<>();
    }


    private void debug(String message) {
        errprintln(String.format("[Directory %d] %s", port, message));
    }

    private void cleanOld() {
        long time = System.currentTimeMillis();
        Iterator it = directory.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<ServerInfo, Long> sinfo = (Map.Entry<ServerInfo, Long>) it.next();
            if (sinfo.getValue() + EXPIRED_TIME_SECONDS * 1000 < time) {
                debug("Unregistering relay node " + sinfo.getKey() + " due to the timeout");
                it.remove();
            }
        }
    }

    // TODO this should probably be asynchronous too...
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            debug(String.format("RUNNING DIRECTORY SERVER ON PORT %d", port));
            while (true) {
                Socket client = serverSocket.accept();
                executor.execute(new ProcessClient(client)::run);
                cleanOld();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static enum QueryType {
        GET_LIST,
        REGISTER
    }

    private class ProcessClient implements Runnable {
        private final Socket socket;

        ProcessClient(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
//            debug("Executing a client");
            try (
                    DataInputStream dis = new DataInputStream(socket.getInputStream());
                    DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
            ) {
                int ordinal = dis.readInt();
                QueryType qt = QueryType.values()[ordinal];
                if (qt == QueryType.GET_LIST) {
                    debug("Sending the list of known relay nodes...");
                    List<ServerInfo> servers = new ArrayList<>(directory.keySet());
                    dos.writeInt(servers.size());
                    for (ServerInfo sinfo: servers) {
                        byte[] bytes = sinfo.getBytes();
                        dos.writeInt(bytes.length);
                        dos.write(bytes);
                    }
                } else { // REGISTER
                    int hostlength = dis.readInt();
                    byte[] bhostname = new byte[hostlength];
                    dis.readFully(bhostname);
                    String hostname = new String(bhostname, Charset.forName("UTF-8"));

                    int port = dis.readInt();

                    int pkeysize = dis.readInt();
                    byte[] bpublicKey = new byte[pkeysize];
                    dis.readFully(bpublicKey);


                    try {
                        PublicKey publicKey = crypto.Tools.getRsaKeyFactory().generatePublic(new X509EncodedKeySpec(bpublicKey));
                        debug(String.format("Registered a relay node %s:%d", hostname, port));
                        ServerInfo key = new ServerInfo(hostname, port, publicKey);
                        long time = System.currentTimeMillis();
                        directory.put(key, time);
                    } catch (InvalidKeySpecException e) {
                        debug("ERROR: bad public key");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("Usage: java DirectoryServer port");
        }
        int port = Integer.parseInt(args[0]);
        new DirectoryServer(port, 1).run(); // TODO threads
        new DirectoryServer(port, 1).run(); // TODO threads
    }
}
