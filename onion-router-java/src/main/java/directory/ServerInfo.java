package directory;


import java.io.*;
import java.nio.charset.Charset;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

public class ServerInfo implements Comparable<ServerInfo> {
    public final String hostname;
    public final int port;
    public final PublicKey rsaPublic;

    public ServerInfo(String hostname, int port, PublicKey rsaPublic) {
        this.hostname = hostname;
        this.port = port;
        this.rsaPublic = rsaPublic;
    }

    public byte[] getBytes() {
        try {
            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            DataOutputStream dos = new DataOutputStream(bs);

            byte[] bhostname = hostname.getBytes(Charset.forName("UTF-8"));
            dos.writeInt(bhostname.length);
            dos.write(bhostname);

            dos.writeInt(port);

            byte[] bytes = rsaPublic.getEncoded();
            dos.writeInt(bytes.length);
            dos.write(bytes);
            return bs.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }



    @Override
    public String toString() {
        return "ServerInfo{" +
                "port=" + port +
                ", hostname='" + hostname + '\'' +
                '}';
    }

    public static ServerInfo fromBytes(byte[] bytes) throws InvalidKeySpecException {
        try {
            DataInputStream dis = new DataInputStream(new ByteArrayInputStream(bytes));

            int hlength = dis.readInt();
            byte[] bhostname = new byte[hlength];
            dis.readFully(bhostname);
            String hostname = new String(bhostname, Charset.forName("UTF-8"));

            int port = dis.readInt();

            int pkeylength = dis.readInt();
            byte[] bpublicKey = new byte[pkeylength];
            dis.readFully(bpublicKey);

            PublicKey publicKey = crypto.Tools.getRsaKeyFactory().generatePublic(new X509EncodedKeySpec(bpublicKey));


            return new ServerInfo(hostname, port, publicKey);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ServerInfo that = (ServerInfo) o;

        if (port != that.port) return false;
        if (!hostname.equals(that.hostname)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = rsaPublic.hashCode();
        result = 31 * result + hostname.hashCode();
        result = 31 * result + port;
        return result;
    }

    @Override
    public int compareTo(ServerInfo o) {
        int c1 = this.hostname.compareTo(o.hostname);
        int c2 = Integer.compare(this.port, o.port);
        if (c1 != 0) {
            return c1;
        } else {
           return c2;
        }
    }
}
