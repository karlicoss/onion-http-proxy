package proxy;

import crypto.DiffieHellman;
import directory.DirectoryServer;
import directory.ServerInfo;
import onion.BackwardOuterPacket;
import onion.ForwardInnerPacket;
import onion.ForwardInnerPacketType;
import onion.OnionServer;
import org.apache.commons.lang3.RandomUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.math.BigInteger;
import java.net.*;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static crypto.DiffieHellman.secretToBytes;
import static tools.Tools.errprintln;

public class HttpProxyServer extends OnionServer {
    public final int port;

    private final Executor clientsExecutor = Executors.newFixedThreadPool(1); // TODO

    private final Set<ServerInfo> servers = new ConcurrentSkipListSet<>();

    private static final int UPDATE_INTERVAL_SECS = 30;

    private final int chainLength;

    private final InetSocketAddress directoryAddress;

    private final List<Connection> connections = new ArrayList<>();
    // available connections

//    public HttpProxyServer(int port, List<InetSocketAddress> chain, int cThreads, int ioThreads) {
//        super(cThreads, ioThreads);
//        this.port = port;
//        this.chain = chain;
//    }

    public HttpProxyServer(int port, InetSocketAddress directoryAddress, int chainLength) {
        super(5, 5);
        this.port = port;
        this.directoryAddress = directoryAddress;
        this.chainLength = chainLength;
    }

    private List<ServerInfo> createRandomChain(int length) {
        List<ServerInfo> snapshot = new ArrayList<>(servers);

        List<ServerInfo> chain = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            int index = RandomUtils.nextInt(0, snapshot.size());
            chain.add(snapshot.get(index));
        }
        return chain;
    }

    /**
     * TODO I guess it better be asynchronous as well
     */
    public void run() {
        updateServers(); // better update at least once at start
        new Thread(this::runServersUpdater).start();

        Connection connection = null;

        ServerSocket serverSocket = null;

        int connectionsCount = 0;

        try {
            serverSocket = new ServerSocket(this.port);
            debug("STARTED ONION HTTP PROXY SERVER ON PORT " + this.port);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                if (connectionsCount % 3 == 0) { // TODO 3
                    debug("Old connection has expired, choosing new connection chain...");
                    connection = new Connection();
                    connection.establish();
                    connections.add(connection);
                }
                clientsExecutor.execute(new HttpProxyHandler(clientSocket, connection)::run);
                connectionsCount += 1;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateServers() {
        try {
            debug("Updating relay nodes list...");

            Socket socket = new Socket();
            socket.connect(directoryAddress);

            DataInputStream dis = new DataInputStream(socket.getInputStream());
            DataOutputStream dos = new DataOutputStream(socket.getOutputStream());

            dos.writeInt(DirectoryServer.QueryType.GET_LIST.ordinal());
            socket.shutdownOutput();

            List<ServerInfo> oldServers = new ArrayList<>(servers);
            List<ServerInfo> newServers = new ArrayList<>();

            int nservers = dis.readInt();
            for (int i = 0; i < nservers; i++) {
                int size = dis.readInt();
                byte[] bytes = new byte[size];
                dis.readFully(bytes);
                try {
                    ServerInfo sinfo = ServerInfo.fromBytes(bytes);
                    newServers.add(sinfo);
                } catch (InvalidKeySpecException e) {
                    debug("ERROR: bad public key");
                }
            }
            servers.addAll(newServers);

            Iterator<ServerInfo> iterator = servers.iterator();
            while (iterator.hasNext()) {
                ServerInfo sinfo = iterator.next();
                if (oldServers.contains(sinfo) && !newServers.contains(sinfo)) {
                    iterator.remove();
                }
            }
            debug("Aware of " + servers.size() + " relay nodes");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void runServersUpdater() {
//        debug("RUNNING RELAY NODES UPDATER");
        while (true) {
            try {
                updateServers();
                Thread.sleep(UPDATE_INTERVAL_SECS * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void debug(String message) {
        errprintln(String.format("[HTTP Proxy %d] %s", port, message));
    }

    /**
     * Gets the query from the client, communicates with the real proxy, responds the client
     */
    public class HttpProxyHandler implements Runnable {
        private final Socket socket;
        private final Connection connection;

        public HttpProxyHandler(Socket socket, Connection connection) {
            this.socket = socket;
            this.connection = connection;
            debug("-----------New client--------------");
        }

        private void debug(String message) {
            System.err.println("[HTTP Proxy] " + message);
        }

        public void run() {
            try (
                    BufferedReader clientIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    OutputStream clientOut = socket.getOutputStream()
            ) {
                String firstLine = clientIn.readLine();
                String requestURL = firstLine.split(" ")[1]; // url should be there
                debug("Client requested " + requestURL);
                debug("Sending GET request into the relay chain");

                byte[] response = connection.communicate(requestURL.getBytes(Charset.forName("UTF-8")));

                debug("Received response!");

                clientOut.write(response);
                clientOut.flush();

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    debug("----------Closing client socket-----------");
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public class Connection {
        private List<ServerInfo> chain = null;
        private List<byte[]> keys = null;


        public Connection() {
        }

        private byte[] encryptRSA(byte[] data, PublicKey publicKey) {
            try {
                final Cipher cipher = Cipher.getInstance("RSA");
                cipher.init(Cipher.ENCRYPT_MODE, publicKey);
                return cipher.doFinal(data);
            } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
                e.printStackTrace();
                return null;
            }
        }

        /**
         * Initiates the connection with each server in the chain sequentially and stores the keys.
         * Returns chain of servers and corresponding keys
         */
        private void establish() {
            chain = createRandomChain(chainLength);
            keys = new ArrayList<>();
            debug("New chain: " + chain.toString());



            keys.add(null); // fake key for the first node

            for (ServerInfo sinfo : chain) {
                InetSocketAddress address = new InetSocketAddress(sinfo.hostname, sinfo.port);

//                debug("Establishing connection with server " + address);
                BigInteger privateDhSecret = DiffieHellman.generatePrivateSecret();
                BigInteger publicDhSecret = DiffieHellman.publicSecret(privateDhSecret);
//                byte[] dh1 = new byte[]{1, 2, 3};// DH first part goes there?
                byte[] dh1 = publicDhSecret.toByteArray();

                byte[] edh1 = encryptRSA(dh1, sinfo.rsaPublic);
                ForwardInnerPacket cpacket = new ForwardInnerPacket(ForwardInnerPacketType.EXTEND, address, edh1);
                byte[] ebpacket = doEncrypt(cpacket);
//                debug("Sending connection request: " + Arrays.toString(ebpacket));
                BackwardOuterPacket response = relay(null, ebpacket, null).join();
                byte[] dh2 = doDecrypt(response.getData());

                BigInteger otherPublicDhSecret = new BigInteger(dh2);
                BigInteger key = DiffieHellman.getKey(privateDhSecret, otherPublicDhSecret);
                debug("Acquired key " + key);
                byte[] bkey = secretToBytes(key);

//                debug(String.format("Received key " + Arrays.toString(key)));
                keys.add(bkey);
//                debug("Established connection with server " + address);
            }

            debug("The connection has been established");
        }

        /**
         * Encrypts data with all current available keys and wraps into InnerPackets
         * @param data
         * @return
         */
        private byte[] doEncrypt(ForwardInnerPacket data) {
            byte[] cur = encrypt(data.getBytes(), keys.get(keys.size() - 1));
            for (int i = keys.size() - 2; i >= 0; i--) {
                cur = encrypt(new ForwardInnerPacket(ForwardInnerPacketType.DATA, null, cur).getBytes(), keys.get(i));
            }
            return cur;
        }

        /**
         * Decrypts data with all current available keys and unwraps InnerPackets
         * @param edata
         * @return
         */
        private byte[] doDecrypt(byte[] edata) {
            byte[] res = edata;
            for (byte[] key : keys) {
                res = decrypt(res, key);
            }
            return res;
        }

        /**
         * Synchronously communicates with the endpoint server (which is the "real" proxy)
         * @param data
         * @return
         */
        public byte[] communicate(byte[] data) {
//            debug(String.format("Sending %s", Arrays.toString(data)));
            ForwardInnerPacket packet = new ForwardInnerPacket(ForwardInnerPacketType.DATA, null, data);
            byte[] ebpacket = doEncrypt(packet);
            BackwardOuterPacket eresponse = relay(null, ebpacket, null).join();
            byte[] response = doDecrypt(eresponse.getData());
//            debug(String.format("Received response: %s", Arrays.toString(response)));
            return response;
        }
    }

    public static void main(String[] args) {
        if (args.length != 4) {
            throw new IllegalArgumentException("Usage: java HttpProxyServer port chainLength directoryHostname directoryPort");
        }
        int port = Integer.parseInt(args[0]);
        int chainLength = Integer.parseInt(args[1]);
        String directoryHostname = args[2];
        int directoryPort = Integer.parseInt(args[3]);
        InetSocketAddress directoryAddress = new InetSocketAddress(directoryHostname, directoryPort);
        new HttpProxyServer(port, directoryAddress, chainLength).run();
    }
}
