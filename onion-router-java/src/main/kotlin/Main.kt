import kotlin.concurrent.thread
import java.util.Timer
import java.util.concurrent.Executors
import java.util.concurrent.Callable
import java.util.concurrent.ExecutionException
import java.net.SocketAddress
import java.net.InetAddress
import java.net.InetSocketAddress
import relay.RelayServer;
import proxy.HttpProxyServer;
import directory.DirectoryServer
import java.util.ArrayList

fun main(args: Array<String>) {
    val proxyPort = 10000;
    val dserverPort = 1567;


    val daddress = InetSocketAddress("localhost", dserverPort);
    val serverPort = 1234

    val dserver = DirectoryServer(dserverPort, 1)
    thread() {
        dserver.run();
    }
    Thread.sleep(500);

    val serversCount = 5
    val servers = ArrayList<RelayServer>()

    for (i in 0.rangeTo(serversCount - 1)) {
        servers.add(RelayServer(serverPort + 1 + i, daddress))
    }
    for (server in servers) {
        thread() {
            server.run()
        }
    }
    Thread.sleep(1000)

    val chainLength = 3
    val proxy = HttpProxyServer(proxyPort, daddress, chainLength);
    thread() {
        proxy.run();
    }
}