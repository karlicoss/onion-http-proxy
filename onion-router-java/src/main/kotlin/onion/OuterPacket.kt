package onion;

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.ByteToMessageDecoder
import io.netty.handler.codec.MessageToByteEncoder
import org.apache.commons.lang3.builder.ReflectionToStringBuilder

enum class PacketType {
    FORWARD,
    BACKWARD,
}

enum class ForwardCommandType {
    CREATE,
    RELAY,
}

enum class BackwardResponseType {
    CREATED,
    RELAY,
    ERROR,
}

class ForwardOuterPacket(
        val tp: ForwardCommandType,
        val token: Token,
        val data: ByteArray
): OuterPacket(tp.ordinal, token, data) {
    override fun toString(): String {
        return ReflectionToStringBuilder.toString(this)!!;
    }
}

class BackwardOuterPacket(
        val tp: BackwardResponseType,
        val token: Token,
        val data: ByteArray
): OuterPacket(tp.ordinal, token, data) {
    override fun toString(): String {
        return ReflectionToStringBuilder.toString(this)!!;
    }
}


/**
    Deserializes outer packages
 */
class OuterPacketDecoder(
        val ptype: PacketType
): ByteToMessageDecoder() {
    override fun decode(ctx: ChannelHandlerContext?, buf: ByteBuf?, out: List<Any>?) {
        if (buf!!.readableBytes() < Integer.BYTES) {
            return
        }
        buf.markReaderIndex()

        val size = buf.readInt()
//        System.err.println("[Decoder] packet size %d".format(size))

        if (buf.readableBytes() < size) {
            buf.resetReaderIndex()
            return
        }

        val bytes = ByteArray(size)
        buf.readBytes(bytes)

        val packet = OuterPacket.fromBytes(bytes, ptype)!!
        val ml = out!! as MutableList<Any>
        ml.add(packet)
//        System.err.println("[Decoder] " + packet)
    }
}

/**
 * Serializes outer packages
 */
class OuterPacketEncoder : MessageToByteEncoder<OuterPacket>() {
    override fun encode(ctx: ChannelHandlerContext?, packet: OuterPacket?, buf: ByteBuf?) {
//        System.err.println("[Encoder] Encoding " + packet)
        packet!!
        buf!!
        val bytes = packet.getBytes()!!
        buf.writeInt(bytes.size)
        buf.writeBytes(bytes)
    }
}