package onion;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder
import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.charset.Charset

enum class ForwardInnerPacketType {
    DATA,
    EXTEND,
}

/**
 * Wrapper for the data being sent from the proxy to the endpoint relay server
 * Serialized:
 * * type             int    (4 bytes)
 * * address length   int    (4 bytes)
 * * address in UTF-8 byte[] (length bytes)
 * * port             int    (4 bytes)
 * * data             byte[] (whatever left)
 */
class ForwardInnerPacket(
        val tp: ForwardInnerPacketType,
        /**
         * address is only useful if type is EXTEND
         */
        val address: InetSocketAddress?,
        val data: ByteArray
) {
    public fun getBytes(): ByteArray {
        val bs = ByteArrayOutputStream()
        val ds = DataOutputStream(bs)
        ds.writeInt(tp.ordinal)
        if (address == null) {
            ds.writeInt(0)
            // no port
        } else {
            val addrbytes = address.getHostString()!!.toByteArray(Charset.forName("UTF-8"))
            ds.writeInt(addrbytes.size)
            ds.write(addrbytes)
            ds.writeInt(address.getPort())
        }
        ds.write(data)
        ds.close()
        return bs.toByteArray()
    }

    override fun toString(): String {
        return ReflectionToStringBuilder.toString(this)!!;
    }

    companion object {
        fun fromBytes(bytes: ByteArray): ForwardInnerPacket {
            val bb = ByteBuffer.wrap(bytes)
            val tp = ForwardInnerPacketType.values()[bb.getInt()]
            val addrlen = bb.getInt()
            val address =
                    if (addrlen == 0) {
                        null
                    } else {
                        val addrbytes = ByteArray(addrlen)
                        bb.get(addrbytes)
                        val hostname = String(addrbytes, Charset.forName("UTF-8"));
                        val port = bb.getInt()
                        InetSocketAddress.createUnresolved(hostname, port)
                    }

            val data = ByteArray(bb.remaining())
            bb.get(data)
            return ForwardInnerPacket(tp, address, data);
        }
    }
}
