package onion;

import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import org.apache.commons.lang3.builder.ReflectionToStringBuilder
import java.nio.ByteBuffer

/**
 * Wrapper for responses from the endpoint relay to the server
 */
class BackwardInnerPacket(
        private val data: ByteArray
) {
    fun getBytes(): ByteArray {
        val bs = ByteArrayOutputStream()
        val ds = DataOutputStream(bs)
        ds.write(data)
        ds.close()
        return bs.toByteArray()
    }

    override fun toString(): String {
        return ReflectionToStringBuilder.toString(this)!!;
    }

    companion  object {
        fun fromBytes(bytes: ByteArray): BackwardInnerPacket {
            val bb = ByteBuffer.wrap(bytes)
            val data = ByteArray(bb.remaining())
            bb.get(data)
            return BackwardInnerPacket(data)
        }
    }
}
