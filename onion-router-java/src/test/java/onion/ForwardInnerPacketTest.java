package onion;

import org.junit.Assert;
import org.junit.Test;

import java.net.InetSocketAddress;

public class ForwardInnerPacketTest {
    @Test
    public void testGetBytesAndFromBytes() throws Exception {
        byte[] data = new byte[]{1, 2, 3, 4, 5};
        InetSocketAddress address = InetSocketAddress.createUnresolved("google.com", 1234);
        ForwardInnerPacket packet = new ForwardInnerPacket(
                ForwardInnerPacketType.DATA,
                address,
                data);
        byte[] bytes = packet.getBytes();
        ForwardInnerPacket packet2 = ForwardInnerPacket.Companion.fromBytes(bytes);
        Assert.assertEquals(packet.getTp(), packet2.getTp());
        Assert.assertEquals(packet.getAddress(), packet2.getAddress());
        Assert.assertArrayEquals(packet.getData(), packet2.getData());
    }

}
