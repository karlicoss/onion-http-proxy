package onion;

import org.junit.Assert;
import org.junit.Test;

public class TokenTest {
    @Test
    public void testGetBytesAndFromBytes() throws Exception {
        Token token = new Token(1);
        byte[] bytes = token.getBytes();
        Token token2 = Token.fromBytes(bytes);
        Assert.assertEquals(token, token2);
    }

}
